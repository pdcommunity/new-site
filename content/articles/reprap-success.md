---
title: رپرپ، تحولی در دنیای پرینتر سه بعدی
layout: article.swig
related:
  - desktop-factory
  - public-data-dream
---

اولین پرینتر سه بعدی در سال ۱۹۸۴ اختراع شد که قیمتی در حدود صد هزار دلار داشت. این
قیمت برای مدت ها تغییر چندانی نکرد
(بیست هزار دلار در ۲۰۱۰)
اما در سال ۲۰۱۳، پرینتر سه بعدی هزار دلاری در بازار موجود بود. چه اتفاقی در این
سال ها افتاد که پرینتر سه بعدی به این شدت ارزان و در دسترس شد؟

[رپرپ (RepRap)](https://reprap.org)
ماشینی که بتواند خودش را به وسیله داده های طراحی اش بازسازی کند. پروژه ای که در سال
۲۰۰۶
در دانشگاه باث آغاز شد. داده های این پروژه از ابتدا داده های عمومی بود؛ از طراحی
پرینتر تا سفت افزار (firmware).
به ویژه داده های مربوط به تولید آن عمومی بود زیرا باید هر کسی می توانست به وسیله
این پرینتر، یک کیت را تولید و سپس با سر هم کردن آن مثل آن را بسازد.

این پروژه به همراه منقضی شدن پتنت تکنولوژی مربوط به این نوع از پرینتر ها، باعث
شد تا پرینتر سه بعدی از یک تکنولوژی گران که تنها برای کمپانی های بزرگ و مصارف خاص
قابل استفاده باشد به یک تکنولوژی در دسترس برای همه تبدیل شود.

رپرپ نشان می دهد که داده های عمومی می تواند ساختار های ناکارآمد و ناعادلانه فعلی
را در هم بشکند و فناوری های پیچیده را برای عموم فراهم کند.

ممکن است فکر کنید که پرینتر سه بعدی یک فناوری ساده است و مثلا به این استناد کنید که
شرکت های کوچکی (حتی در ایران) قادر به ساخت پرینتر سه بعدی هستند و داده های عمومی در
فناوری های پیچیده نمی تواند موفق شود. اما شما در تشخیص علت و معلول اشتباه کرده اید. داده
های عمومی باعث شده اند که پرینتر سه بعدی ساده باشد و تمام پرینتر های سه بعدی که شرکت
های کوچک می سازند بر پایه رپرپ ها یا دیگر پرینتر های عمومی هستند. پس چون داده های عمومی
به حوزه پرینتر سه بعدی وارد شده اند، پرینتر سه بعدی یک فناوری ساده و در دسترس شده است
نه برعکس و ممکن بود که مثلا پردازنده ها یک فناوری ساده شوند و پرینتر سه بعدی یک فناوری
پیچیده بماند.

سوالی که در این جا به وجود می آید این است که چرا این اتفاق برای فناوری های دیگر، مثلا
همان پردازنده ها نمی افتد؟ یعنی در طول این هشتاد سالی که بشر بر روی پردازنده ها کار
می کرده، هیچ کس به این فکر نیفتاده که یک پردازنده با داده های عمومی بسازد؟ دو نکته
این موضوع را توضیح می دهد:
* دیگر برای اینکار دیر شده است: فرض کنید پروژه رپرپ بیست سال دیرتر شروع می شد. می شد
انتظار داشت که قیمت پرینتر سه بعدی چهار هزار دلار شود و سرعت و دقت آن چند برابر شود. حال
رپرپی که تازه می خواست شروع شود، حتی با قیمت هزار دلار نمی توانست با پرینتر های انحصاری
رقابت کند و برای این که در زمان کم به سرعت و دقت پرینتر های دیگر برسد به سرمایه عظیمی
نیاز داشت. سرمایه ای که تنها در اختیار سرمایه دارانی است که برای سود بیشتر، داده های
خود را انحصار می کنند. این اتفاق برای پردازنده ها افتاده است.
* هم افزایی انحصار: در برهه خاصی از زمان، سیستم عامل های دسکتاپ در انحصار شرکت مایکروسافت
قرار گرفت که محصولاتش را تنها روی معماری
x86
اینتل ارائه می دهد و در نتیجه بازیگرانی که اجازه ساخت این نوع پردازنده ها را نداشتند
(همه به جز AMD و خود اینتل)
به مرور از صحنه حذف شدند.
در حالی که اگر یک سیستم عامل عمومی مانند لینوکس فراگیر می شد، هر کسی می توانست آن را
برای معماری خود استفاده کند و حتی اگر پردازنده عمومی به وجود نمی آمد، رقابت می توانست
پردازنده ها را ارزان تر و با کیفیت تر کند.
